﻿<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="12.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <UsingTask TaskName="ValidateUpdateScripts" TaskFactory="CodeTaskFactory" AssemblyFile="$(MSBuildBinPath)\Microsoft.Build.Tasks.v12.0.dll">
    <ParameterGroup>
      <ConnectionString ParameterType="System.String" Required="true" />
      <Scripts ParameterType="Microsoft.Build.Framework.ITaskItem[]" Required="true" />
      <Results ParameterType="Microsoft.Build.Framework.ITaskItem[]" Output="true" />
    </ParameterGroup>
    <Task>
      <Reference Include="System.Data" />
      <Code Type="Class" Language="cs">
        <![CDATA[
namespace ValidateUpdateScriptsTask
{
    using Microsoft.Build.Framework;
    using Microsoft.Build.Utilities;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Text.RegularExpressions;

    public class ValidateUpdateScripts : Microsoft.Build.Utilities.Task
    {
        private static readonly Regex _validatingQueryPattern = new Regex("^(.+?\n)?--[ \t]*beginvalidatingquery[ \t]*\r?\n(?<validatingquery>.*?)\n[ \t]*--endvalidatingquery.*$", RegexOptions.Singleline | RegexOptions.ExplicitCapture);


        public virtual string ConnectionString { get; set; }

        public virtual ITaskItem[] Scripts { get; set; }

        public virtual ITaskItem[] Results { get; set; }


        public override bool Execute()
        {
            var results = new List<ITaskItem>();

            foreach (var script in Scripts)
            {
                try
                {
                    var content = File.ReadAllText(script.ItemSpec);
                    var query = _validatingQueryPattern.Match(content).Groups["validatingquery"].Value;

                    if (string.IsNullOrWhiteSpace(query))
                    {
                        Log.LogError("No validating query found in {0}.", script.ItemSpec);
                        return false;
                    }

                    var status = -1;
                    var message = "Query not executed.";

                    using (var connection = new SqlConnection(ConnectionString))
                    {
                        var command = connection.CreateCommand();
                        command.CommandType = CommandType.Text;
                        command.CommandText = query;

                        connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            if (!reader.Read())
                            {
                                throw new Exception("No rows returned.");
                            }

                            status = reader.GetInt32(0);
                            message = reader.GetString(1);

                            if (reader.Read())
                            {
                                throw new Exception("Multiple rows returned.");
                            }
                        }
                    }

                    if (status != 0 && status != 1)
                    {
                        Log.LogError("Error from validating query for {0}: {1}", script.ItemSpec, message);
                        return false;
                    }

                    var resultItem = new TaskItem(script);
                    resultItem.SetMetadata("Execute", status == 0 ? "false" : "true");
                    resultItem.SetMetadata("Message", message);

                    results.Add(resultItem);
                }
                catch (Exception ex)
                {
                    Log.LogError("Error executing validating query for {0}: {1}", script.ItemSpec, ex.Message);
                    return false;
                }
            }

            Results = results.ToArray();
            return true;
        }
    }
}
        ]]>
      </Code>
    </Task>
  </UsingTask>
</Project>
