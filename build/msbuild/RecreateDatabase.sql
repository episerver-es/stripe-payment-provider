if '$(CreateUser)' = 'true' and not exists (select 1 from sys.sql_logins where name = '$(DatabaseUser)')
begin
    create login [$(DatabaseUser)] with password=N'$(DatabasePassword)', default_database=[master], check_expiration=off, check_policy=off
end
go

-- with localdb, the database will continue to exist after the database file has been deleted; and
-- the drop database command will report an error (but the database will be dropped). give localdb
-- databases a first chance to drop with errors suppressed; if a different error occurs and the
-- database is not dropped, the next drop database will report it.
if exists (select 1 from sys.databases where name = '$(DatabaseName)') and '$(IsLocalDb)' = 'true'
        begin try
            drop database [$(DatabaseName)]
        end try
        begin catch
        end catch
go

if exists (select 1 from sys.databases where name = '$(DatabaseName)')
    drop database [$(DatabaseName)]
go

if '$(IsLocalDb)' = 'true'
    create database [$(DatabaseName)]
    on primary (name=$(DataFileName), filename='$(DataFile)')
    log on (name=$(LogFileName), filename='$(LogFile)')
else
    create database [$(DatabaseName)]
go

if '$(DatabaseCollation)' != ''
begin
    declare @alter_collation nvarchar(4000) = 'alter database [$(DatabaseName)] collate $(DatabaseCollation)'
    exec dbo.sp_executesql @alter_collation
end
go

use [$(DatabaseName)]
go

if '$(CreateUser)' = 'true'
begin
    create user [$(DatabaseUser)] for login [$(DatabaseUser)]
    alter role [db_owner] add member [$(DatabaseUser)]
end
go
