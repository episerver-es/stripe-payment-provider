﻿using EPiServer.Commerce.Catalog;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.Linking;
using EPiServer.Commerce.SampleMvc.Models.Catalog;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Core;
using EPiServer.Filters;
using Mediachase.Commerce;
using Mediachase.Commerce.Markets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Web;

namespace EPiServer.Commerce.SampleMvc.Business
{
    /// <summary>
    /// Creates view models
    /// </summary>
    public class ViewModelFactory
    {
        private const string DefaultWarehouseCode = "default";

        private readonly ICurrentMarket _currentMarket;
        private readonly InventoryLoader _inventoryLoader;
        private readonly ReadOnlyPricingLoader _readOnlyPricingLoader;
        private readonly IContentLoader _contentLoader;
        private readonly ILanguageSelector _languageSelector;
        private readonly ILinksRepository _linksRepository;
        private readonly IMarketService _marketService;
        private readonly IContentRepository _contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelFactory"/> class.
        /// </summary>
        /// <param name="contentLoader">The content loader.</param>
        /// <param name="languageSelector">The language selector.</param>
        /// <param name="currentMarket">The current market service.</param>
        /// <param name="inventoryLoader">The inventory loader.</param>
        /// <param name="readOnlyPricingLoader">The read only pricing loader.</param>
        /// <param name="linksRepository">The links repository.</param>
        /// <param name="contentViewModelFactory">The content view model factory.</param>
        public ViewModelFactory(
            IContentLoader contentLoader,
            ILanguageSelector languageSelector,
            ICurrentMarket currentMarket,
            InventoryLoader inventoryLoader,
            ReadOnlyPricingLoader readOnlyPricingLoader,
            ILinksRepository linksRepository,
            IMarketService marketService,
            IContentRepository contentRepository)
        {
            _contentLoader = contentLoader;
            _languageSelector = languageSelector;
            _currentMarket = currentMarket;
            _inventoryLoader = inventoryLoader;
            _readOnlyPricingLoader = readOnlyPricingLoader;
            _linksRepository = linksRepository;
            _marketService = marketService;
            _contentRepository = contentRepository;
        }

        /// <summary>
        /// Creates a page view model.
        /// </summary>
        /// <param name="pageData">The page data.</param>
        /// <returns>A page view model instance.</returns>
        public IPageViewModel<PageData> CreatePageViewModel(PageData pageData)
        {
            var activator = new Activator<IPageViewModel<PageData>>();
            var model = activator.Activate(typeof(PageViewModel<>), pageData);
            InitializePageViewModel(model);
            return model;
        }

        /// <summary>
        /// Creates a catalog view model instance.
        /// </summary>
        /// <param name="catalogContent">The catalog content.</param>
        /// <param name="pageData">The page data.</param>
        /// <returns>A catalog content view model instance.</returns>
        public ICatalogViewModel<CatalogContentBase, PageData> CreateCatalogViewModel(CatalogContentBase catalogContent, PageData pageData)
        {
            var activator = new Activator<ICatalogViewModel<CatalogContentBase, PageData>>();
            var model = activator.Activate(typeof(CatalogContentViewModel<,>), catalogContent, pageData);
            InitializeCatalogViewModel(model);
            return model;
        }

        /// <summary>
        /// Creates a product view model instance.
        /// </summary>
        /// <param name="productContent">The product content.</param>
        /// <param name="pageData">The page data.</param>
        /// <returns>A catalog content view model instance.</returns>
        public IProductViewModel<ProductContent, PageData> CreateProductViewModel(ProductContent productContent, PageData pageData)
        {
            var activator = new Activator<IProductViewModel<ProductContent, PageData>>();
            var model = activator.Activate(typeof(ProductViewModel<,>), productContent, pageData);
            InitializeCatalogViewModel(model);
            return model;
        }

        public IVariationViewModel<TContent, HomePage> CreateVariationViewModel<TContent>(TContent variationContent)
            where TContent : VariationContent
        {
            var homePage = _contentRepository.Get<HomePage>(SiteDefinition.Current.StartPage);
            return CreateVariationViewModel<TContent, HomePage>(variationContent, homePage);
        }

        public IVariationViewModel<TContent, TPage> CreateVariationViewModel<TContent, TPage>(TContent variationContent, TPage pageData)
            where TContent : VariationContent
            where TPage : PageData
        {
            var activator = new Activator<IVariationViewModel<TContent, TPage>>();
            var model = activator.Activate(typeof(VariationViewModel<TContent, TPage>), variationContent, pageData);
            InitializeVariationViewModel(model);
            return model;
        }

        public FashionProductViewModel CreateFashionViewModel(FashionProductContent currentContent)
        {
            var homePage = _contentRepository.Get<HomePage>(ContentReference.StartPage);
            var model = new FashionProductViewModel(currentContent, homePage);
            InitializeCatalogViewModel(model);
            return model;
        }

        public MediaProductViewModel CreateMediaViewModel(MediaProductContent currentContent)
        {
            var homePage = _contentRepository.Get<HomePage>(ContentReference.StartPage);
            var model = new MediaProductViewModel(currentContent, homePage);
            InitializeCatalogViewModel(model);
            return model;
        }

        public void InitializePageViewModel<TViewModel>(TViewModel model)
            where TViewModel : IPageViewModel<PageData>
        {
            model.TopMenuContent = model.TopMenuContent ?? GetTopMenuContent();
            model.MarketSelector = model.MarketSelector ?? GetMarketListItems();
            model.CurrentMarket = model.CurrentMarket ?? _currentMarket.GetCurrentMarket();
        }

        public void InitializeCatalogViewModel<TViewModel>(TViewModel model)
            where TViewModel : ICatalogViewModel<CatalogContentBase, PageData>
        {
            model.ChildCategories = model.ChildCategories ?? GetChildNodes(model.CatalogContent.ContentLink);
            model.Products = model.Products ?? CreateLazyProductContentViewModels(model.CatalogContent, model.CurrentPage);
            model.Variants = model.Variants ?? CreateLazyVariantContentViewModels(model.CatalogContent, model.CurrentPage);

            InitializePageViewModel(model);
        }

        public void InitializeVariationViewModel<TViewModel>(TViewModel model)
            where TViewModel : IVariationViewModel<VariationContent, PageData>
        {
            model.Inventory = model.Inventory ?? GetInventory(model.CatalogContent, DefaultWarehouseCode);
            model.Price = model.Price ?? GetPrices(model.CatalogContent);

            InitializePageViewModel(model);
        }

        private IEnumerable<SelectListItem> GetMarketListItems()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var market in _marketService.GetAllMarkets())
            {
                items.Add(new SelectListItem { Text = market.MarketName, Value = market.MarketId.Value, Selected = market.MarketName == _currentMarket.GetCurrentMarket().MarketName });
            }

            return items;
        }

        private Lazy<IEnumerable<NodeContent>> GetChildNodes(ContentReference contentLink)
        {
            return new Lazy<IEnumerable<NodeContent>>(() =>
                _contentLoader.GetChildren<NodeContent>(contentLink)
                .ToList());
        }

        private Lazy<Inventory> GetInventory(IStockPlacement stockPlacement, string warehouseCode)
        {
            return new Lazy<Inventory>(() => stockPlacement.GetStockPlacement(_inventoryLoader).FirstOrDefault(x => x.WarehouseCode == warehouseCode), true);
        }

        private Lazy<Price> GetPrices(IPricing pricing)
        {
            return new Lazy<Price>(() => pricing.GetPrices(_readOnlyPricingLoader).FirstOrDefault(x => x.MarketId == _currentMarket.GetCurrentMarket().MarketId), true);
        }

        private IEnumerable<IContent> GetTopMenuContent()
        {
            return FilterForVisitor.Filter(_contentLoader.GetChildren<PageData>(ContentReference.StartPage)
                .Where(p => p.VisibleInMenu));
        }

        private LazyProductViewModelCollection CreateLazyProductContentViewModels(CatalogContentBase catalogContent, PageData pageData)
        {
            return new LazyProductViewModelCollection(() =>
            {
                var products = GetChildrenAndRelatedEntries<ProductContent>(catalogContent);
                return products.Select(x => CreateProductViewModel(x, pageData));
            });
        }

        private LazyVariationViewModelCollection CreateLazyVariantContentViewModels(CatalogContentBase catalogContent, PageData pageData)
        {
            return new LazyVariationViewModelCollection(() =>
            {
                var variants = GetChildrenAndRelatedEntries<VariationContent>(catalogContent);
                return variants.Select(x => CreateVariationViewModel<VariationContent, PageData>(x, pageData));
            });
        }

        private IEnumerable<TEntryContent> GetChildrenAndRelatedEntries<TEntryContent>(CatalogContentBase catalogContent)
            where TEntryContent : EntryContentBase
        {
            var variantContentItems = _contentLoader.GetChildren<TEntryContent>(catalogContent.ContentLink).ToList();

            var variantContainer = catalogContent as IVariantContainer;
            if (variantContainer != null)
            {
                variantContentItems.AddRange(GetRelatedEntries<TEntryContent>(variantContainer));
            }

            return variantContentItems.Where(e => e.IsAvailableInCurrentMarket(_currentMarket));
        }

        private IEnumerable<TEntryContent> GetRelatedEntries<TEntryContent>(IVariantContainer content)
            where TEntryContent : EntryContentBase
        {
            var relatedItems = content.GetVariantRelations(_linksRepository).Select(x => x.Target);
            return _contentLoader.GetItems(relatedItems, _languageSelector).OfType<TEntryContent>();
        }

    }
}