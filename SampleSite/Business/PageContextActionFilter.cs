﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Core;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Business
{
    public class PageContextActionFilter : IResultFilter
    {
        private readonly ViewModelFactory _modelFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="PageContextActionFilter"/> class.
        /// </summary>
        /// <param name="modelFactory">The view model factory.</param>
        public PageContextActionFilter(ViewModelFactory modelFactory)
        {
            _modelFactory = modelFactory;
        }

        /// <summary>
        /// Called before an action result executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model;

            var pageViewModel = viewModel as IPageViewModel<PageData>;
            if (pageViewModel != null)
            {
                _modelFactory.InitializePageViewModel(pageViewModel);
            }

            var catalogContentViewModel = viewModel as ICatalogViewModel<CatalogContentBase, PageData>;
            if (catalogContentViewModel != null && catalogContentViewModel.CatalogContent != null)
            {
                _modelFactory.InitializeCatalogViewModel(catalogContentViewModel);
            }

            var variationContentViewModel = viewModel as IVariationViewModel<VariationContent, PageData>;
            if (variationContentViewModel != null && variationContentViewModel.CatalogContent != null)
            {
                _modelFactory.InitializeVariationViewModel(variationContentViewModel);
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}