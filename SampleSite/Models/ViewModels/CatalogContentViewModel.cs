﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;
using System;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class CatalogContentViewModel<T, T1> : PageViewModel<T1>, ICatalogViewModel<T, T1> 
        where T : CatalogContentBase
        where T1 : PageData
    {
        public CatalogContentViewModel(T currentContent, T1 currentPage) : base(currentPage)
        {
            CatalogContent = currentContent;
            CurrentPage = currentPage;
        }

        public T CatalogContent { get; set; }
        public Lazy<IEnumerable<NodeContent>> ChildCategories { get; set; }
        public LazyProductViewModelCollection Products { get; set; }
        public LazyVariationViewModelCollection Variants { get; set; }
    }
}