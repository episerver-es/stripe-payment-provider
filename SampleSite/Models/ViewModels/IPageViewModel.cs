﻿using EPiServer.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Web.Mvc;
using Mediachase.Commerce;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public interface IPageViewModel<out T> 
        where T : PageData
    {
        T CurrentPage { get; }
        IEnumerable<IContent> TopMenuContent { get; set; }
        IEnumerable<SelectListItem> MarketSelector { get; set; }
        IMarket CurrentMarket { get; set; }
    }
}
