﻿using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Website.Helpers;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class WishListModel : PageViewModel<WishListPage>
    {
        public WishListModel(WishListPage currentPage)
            : base(currentPage)
        {
        }

        public IEnumerable<LineItem> LineItems
        {
            get
            {
                var wishListHelper = new CartHelper(CartHelper.WishListName);
                bool isEmpty = wishListHelper.IsEmpty;

                // Make sure to check that prices has not changed
                if (!isEmpty)
                {

                    wishListHelper.Reset();
                    wishListHelper.Cart.ProviderId = "FrontEnd";
                    wishListHelper.RunWorkflow("CartValidate");

                    // If cart is empty, remove it from the database
                    isEmpty = wishListHelper.IsEmpty;
                    if (isEmpty)
                    {
                        wishListHelper.Delete();
                    }
                    wishListHelper.Cart.AcceptChanges();
                }

                return wishListHelper.LineItems;
            }
        }
    }
}