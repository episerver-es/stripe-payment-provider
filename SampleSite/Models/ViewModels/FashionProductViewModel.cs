﻿using EPiServer.Commerce.SampleMvc.Models.Catalog;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class FashionProductViewModel : ProductViewModel<FashionProductContent, HomePage>
    {
        public FashionProductViewModel(FashionProductContent currentContent, HomePage currentPage)
            : base(currentContent, currentPage)
        {
        }

        public IEnumerable<SelectListItem> Color { get; set; }
        public IEnumerable<SelectListItem> Size { get; set; }

        public IVariationViewModel<FashionItemContent, HomePage> FashionItemViewModel { get; set; }
    }
}