﻿using EPiServer.Core;
using Mediachase.Commerce;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class PageViewModel<T> : IPageViewModel<T>
        where T : PageData
    {
        public PageViewModel(T currentPage)
        {
            CurrentPage = currentPage;
        }

        public T CurrentPage
        {
            get;
            set;
        }

        public IEnumerable<IContent> TopMenuContent { get; set; }
        public IEnumerable<SelectListItem> MarketSelector { get; set; }
        public IMarket CurrentMarket { get; set; }
    
    }
}