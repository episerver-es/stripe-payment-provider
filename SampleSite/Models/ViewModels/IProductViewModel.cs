﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public interface IProductViewModel<out T, out TPage> : ICatalogViewModel<T, TPage>
        where T : ProductContent
        where TPage : PageData
    {
    }
}
