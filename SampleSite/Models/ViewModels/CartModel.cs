﻿using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Website.Helpers;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class CartModel : PageViewModel<CartSimpleModulePage>
    {
        public CartModel(CartSimpleModulePage currentPage)
            : base(currentPage)
        {
        }

        public IEnumerable<LineItem> LineItems
        {
            get
            {
                var cartHelper = new CartHelper(Cart.DefaultName);
                bool isEmpty = cartHelper.IsEmpty;

                // Make sure to check that prices has not changed
                if (!isEmpty)
                {
                    cartHelper.Cart.ProviderId = "FrontEnd";
                    cartHelper.RunWorkflow("CartValidate");

                    // If cart is empty, remove it from the database
                    isEmpty = cartHelper.IsEmpty;
                    if (isEmpty)
                    {
                        cartHelper.Delete();
                    }

                    cartHelper.Cart.AcceptChanges();
                }

                return cartHelper.LineItems;
            }
        }
    }
}