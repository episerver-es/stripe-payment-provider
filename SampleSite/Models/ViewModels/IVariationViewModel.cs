﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Core;
using System;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public interface IVariationViewModel<out TVariationContent, out TPageData> : IBaseCatalogViewModel<TVariationContent, TPageData>
        where TVariationContent : VariationContent
        where TPageData : PageData
    {
        Lazy<Inventory> Inventory { get; set; }
        Lazy<Price> Price { get; set; }
    }
}
