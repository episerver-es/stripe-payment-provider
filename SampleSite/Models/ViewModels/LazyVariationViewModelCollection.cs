﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;
using System;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class LazyVariationViewModelCollection : Lazy<IEnumerable<IVariationViewModel<VariationContent, PageData>>>
    {
        public LazyVariationViewModelCollection(Func<IEnumerable<IVariationViewModel<VariationContent, PageData>>> valueFactory)
            : base(valueFactory)
        {
        }
    }
}