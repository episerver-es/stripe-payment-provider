﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;
using System;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class LazyProductViewModelCollection : Lazy<IEnumerable<IProductViewModel<ProductContent, PageData>>>
    {
        public LazyProductViewModelCollection(Func<IEnumerable<IProductViewModel<ProductContent, PageData>>> valueFactory)
            : base(valueFactory)
        {
        }
    }
}