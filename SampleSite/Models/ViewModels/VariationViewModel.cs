﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Core;
using System;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class VariationViewModel<TVariationContent, TPageData> : PageViewModel<TPageData>, IVariationViewModel<TVariationContent, TPageData>
        where TVariationContent : VariationContent
        where TPageData : PageData
    {
        public VariationViewModel(TVariationContent currentContent, TPageData currentPage)
            : base(currentPage)
        {
            CatalogContent = currentContent;
        }

        public TVariationContent CatalogContent { get; private set; }

        public Lazy<Inventory> Inventory { get; set; }
        public Lazy<Price> Price { get; set; }
    }
}