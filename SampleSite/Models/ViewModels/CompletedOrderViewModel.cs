﻿using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class CompletedOrderViewModel : PageViewModel<SingleShipmentCheckoutPage>
    {
        public virtual string PageTitle { get; set; }

        public virtual int PurchaseOrderId { get; set; }

        public virtual string TrackingNumber { get; set; }


        public CompletedOrderViewModel(SingleShipmentCheckoutPage currentPage, ViewModelFactory viewModelFactory)
            : base(currentPage)
        {
            PageTitle = currentPage == null ? "Single Shipment Checkout" : currentPage.PageTitle;
            viewModelFactory.InitializePageViewModel(this);
        }
    }
}