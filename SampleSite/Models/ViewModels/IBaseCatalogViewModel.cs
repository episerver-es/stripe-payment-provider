﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public interface IBaseCatalogViewModel<out T, out TPage> : IPageViewModel<TPage>
        where T : CatalogContentBase
        where TPage : PageData
    {
        T CatalogContent { get; }
    }
}