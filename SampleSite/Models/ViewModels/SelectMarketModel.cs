﻿using EPiServer.ServiceLocation;
using Mediachase.Commerce;
using Mediachase.Commerce.Markets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class SelectMarketModel
    {
        private readonly Injected<IMarketService> _marketService;
        private readonly Injected<ICurrentMarket> _currentMarket;

        public IEnumerable<IMarket> Markets
        {
            get { return _marketService.Service.GetAllMarkets(); }
        }

        public bool IsCurrentMarket(IMarket market)
        {
            return market.MarketId == _currentMarket.Service.GetCurrentMarket().MarketId;
        }
    }
}