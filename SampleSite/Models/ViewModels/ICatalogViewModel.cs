﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;
using System;
using System.Collections.Generic;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public interface ICatalogViewModel<out T, out TPage> : IBaseCatalogViewModel<T, TPage>
        where T : CatalogContentBase
        where TPage : PageData
    {
        Lazy<IEnumerable<NodeContent>> ChildCategories { get; set; }
        LazyProductViewModelCollection Products { get; set; }
        LazyVariationViewModelCollection Variants { get; set; }
    }
}
