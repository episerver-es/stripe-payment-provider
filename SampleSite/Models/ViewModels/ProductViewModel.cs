﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Core;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class ProductViewModel<TProductContent, TPageData> : CatalogContentViewModel<TProductContent, TPageData>, IProductViewModel<TProductContent, TPageData>
        where TProductContent : ProductContent
        where TPageData : PageData
    {
        public ProductViewModel(TProductContent currentContent, TPageData currentPage)
            : base(currentContent, currentPage)
        {
        }
    }
}