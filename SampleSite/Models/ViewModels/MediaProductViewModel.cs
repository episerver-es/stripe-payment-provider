﻿using System.Collections.Generic;
using System.Web.Mvc;
using EPiServer.Commerce.SampleMvc.Models.Catalog;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class MediaProductViewModel : ProductViewModel<MediaProductContent, HomePage>
    {
        public MediaProductViewModel(MediaProductContent currentContent, HomePage currentPage)
            : base(currentContent, currentPage)
        {
        }

        public IEnumerable<SelectListItem> MediaType { get; set; }

        public IVariationViewModel<MediaItemContent, HomePage> MediaItemViewModel { get; set; }
    }
}