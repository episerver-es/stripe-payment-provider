﻿using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class CheckoutViewModel : PageViewModel<SingleShipmentCheckoutPage>
    {
        public virtual string PageTitle { get; set; }

        public virtual Dictionary<string, string> Actions { get; set; }

        public virtual IContent ShoppingOverviewPage { get; set; }

        public IList<KeyValuePair<Guid, string>> PaymentProviders { get; set; }

        public CheckoutViewModel(SingleShipmentCheckoutPage currentPage, ViewModelFactory viewModelFactory)
            : base(currentPage)
        {
            PageTitle = currentPage == null ? "Single Shipment Checkout" : currentPage.PageTitle;
            viewModelFactory.InitializePageViewModel(this);
            ShoppingOverviewPage = TopMenuContent.FirstOrDefault(x => string.Equals(x.Name, "Shopping", StringComparison.OrdinalIgnoreCase));
            Actions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }
    }
}