﻿using EPiServer;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EPiServer.Commerce.SampleMvc.Models.ViewModels
{
    public class CartItemModel
    {
        private static string _cartLink;
        private static string _wishListLink;
        private Injected<IContentLoader> _contentLoader;
        private Injected<UrlResolver> _urlResolver;

        public CartItemModel(EntryContentBase entry)
        {
            Code = entry.Code;
            Entry = entry;
        }

        public string Code
        {
            get;
            set;
        }

        public EntryContentBase Entry
        {
            get;
            set;
        }

        public bool CanBuyEntry
        {
            get { return Entry is VariationContent; }
        }

        public string AddToCartLink
        {
            get 
            { 
                if (_cartLink == null)
                {
                    var homePage = _contentLoader.Service.Get<HomePage>(ContentReference.StartPage);
                    var cartPageLink = homePage.Settings.CartPage;
                    _cartLink = _urlResolver.Service.GetUrl(cartPageLink) + "AddToCart";
                }
                return _cartLink;
            }
        }

        public string AddToWishListLink
        {
            get
            {
                if (_wishListLink == null)
                {
                    var homePage = _contentLoader.Service.Get<HomePage>(ContentReference.StartPage);
                    var cartPageLink = homePage.Settings.WishListPage;
                    _wishListLink = _urlResolver.Service.GetUrl(cartPageLink) + "AddToWishList";
                }
                return _wishListLink;
            }
        }
    }
}