﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using Mediachase.Commerce;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace EPiServer.Commerce.SampleMvc.Models
{
    public static class CommerceHtmlHelperExtensions
    {
        public static MvcHtmlString InStockRange(this HtmlHelper html, LazyVariationViewModelCollection variationViewModels)
        {
            var variationViewModelsList = variationViewModels.Value.ToList();
            if (!variationViewModelsList.Any())
            {
                return new MvcHtmlString("There is no stock information");
            }

            var minInStock = variationViewModelsList.Min(x => x.Inventory.Value != null ? x.Inventory.Value.InStockQuantity : 0);
            var maxInStock = variationViewModelsList.Max(x => x.Inventory.Value != null ? x.Inventory.Value.InStockQuantity : 0);

            if (maxInStock.Equals(minInStock))
            {
                return new MvcHtmlString(GetInStockString(maxInStock));
            }

            return new MvcHtmlString(String.Concat(GetInStockString(minInStock), " - ", GetInStockString(maxInStock), " pending on variant."));
        }

        public static MvcHtmlString UnitPriceRange(this HtmlHelper html, LazyVariationViewModelCollection variationViewModels)
        {
            var variationViewModelsList = variationViewModels.Value.ToList();
            if (!variationViewModelsList.Any())
            {
                return new MvcHtmlString("There is no price information");
            }

            var minPrice = variationViewModelsList.Min(x => x.Price.Value != null ? x.Price.Value.UnitPrice : default(Money));
            var maxPrice = variationViewModelsList.Max(x => x.Price.Value != null ? x.Price.Value.UnitPrice : default(Money));

            if (maxPrice.Equals(minPrice))
            {
                return new MvcHtmlString(GetPriceString(maxPrice));
            }

            return new MvcHtmlString(String.Concat(GetPriceString(minPrice), " - ", GetPriceString(maxPrice), " pending on variant."));
        }

        private static string GetInStockString(decimal inStock)
        {
            return inStock.ToString(CultureInfo.CurrentCulture);
        }

        private static string GetPriceString(Money price)
        {
            return price.ToString(CultureInfo.CurrentCulture);
        }

        public static MvcHtmlString AssetImage(this HtmlHelper html, EntryContentBase entry)
        {
            var commerceMedia = GetCommerceMedia(entry);
            if (commerceMedia == null)
            {
                return MvcHtmlString.Empty;
            }

            return html.Partial(Web.UIHint.Image, commerceMedia.AssetLink);
        }

        public static MvcHtmlString AssetUrl(this HtmlHelper html, EntryContentBase entry)
        {
            return AssetUrl(html, entry, ServiceLocator.Current.GetInstance<UrlResolver>());
        }

        public static MvcHtmlString AssetUrl(this HtmlHelper html, EntryContentBase entry, UrlResolver urlResolver)
        {
            var commerceMedia = GetCommerceMedia(entry);
            if (commerceMedia == null)
            {
                return new MvcHtmlString(null);
            }

            return AssetUrl(commerceMedia.AssetLink, urlResolver);
        }

        public static MvcHtmlString AssetUrl(this HtmlHelper html, EntryContentBase entry, int index)
        {
            return AssetUrl(html, entry, index, ServiceLocator.Current.GetInstance<UrlResolver>());
        }

        public static MvcHtmlString AssetUrl(this HtmlHelper html, EntryContentBase entry, int index, UrlResolver urlResolver)
        {
            var commerceMedia = GetCommerceMedia(entry, index);
            if (commerceMedia == null)
            {
                return new MvcHtmlString(null);
            }

            return AssetUrl(commerceMedia.AssetLink, urlResolver);
        }

        private static CommerceMedia GetCommerceMedia(EntryContentBase entry)
        {
            return GetCommerceMedia(entry, 0);
        }

        private static CommerceMedia GetCommerceMedia(EntryContentBase entry, int index)
        {
            return entry.CommerceMediaCollection.OrderBy(m => m.SortOrder).Skip(index).FirstOrDefault();
        }

        private static MvcHtmlString AssetUrl(ContentReference assetLink, UrlResolver urlResolver)
        {
            return new MvcHtmlString(urlResolver.GetUrl(assetLink));
        }
    }
}