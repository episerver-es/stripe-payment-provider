﻿using EPiServer.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.PageTypes
{
    [ContentType(GUID = "3B579852-D4AD-41D5-B4BA-40FF4CC55A6B",
        DisplayName = "Not Supported Market Page",
        Description = "",
        GroupName = "CommerceSample",
        Order = 100)]
    public class MarketNotSupportedPage : CommerceSampleModulePage
    {

    }
}