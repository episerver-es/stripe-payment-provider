﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.PageTypes
{
    [ContentType(GUID = "707A88B4-890C-4E43-9E65-95B3ED493073",
        DisplayName = "Truffler SAAS Search Page",
        GroupName = "CommerceSample",
        Order = 100,
        Description = "The page shows the result by searching Truffler SAAS.")]
    public class SearchTrufflerSAASPage : CommerceSampleModulePage
    {
       
    }
}