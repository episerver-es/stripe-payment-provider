﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "8D2FAA84-28E5-41D4-9AF7-A867574B408F", MetaClassName = "Media_Product_Class")]
    public class MediaProductContent: ProductContent
    {
    }
}