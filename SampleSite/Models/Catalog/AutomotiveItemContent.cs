﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "CB06EC73-D19A-4BD5-BB87-844B2FB884C4", MetaClassName = "Automotive_Item_Class")]
    public class AutomotiveItemContent : VariationContent
    {
    }
}