﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;
using EPiServer.Core;
using System.ComponentModel.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "7AB5BB63-9B81-4D40-91FA-5551F985004D", MetaClassName = "DepartmentNodeWithProducts")]
    public class DepartmentNodeWithProductsContent : NodeContent
    {
        [Display(Name = "Link Reference", Order = 99)]
        [UIHint(UIHint.AllContent)]
        public virtual ContentReference LinkReference { get; set; }
    }
}