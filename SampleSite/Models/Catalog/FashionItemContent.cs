﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;
using EPiServer.Core;
using System.ComponentModel.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "BE40A3E0-49BC-48DD-9C1D-819C2661C9BD", MetaClassName = "Fashion_Item_Class")]
    public class FashionItemContent : VariationContent
    {
        [Display(Name = "Size")]
        public virtual string Facet_Size { get; set; }

        [Display(Name = "Color")]
        public virtual string Facet_Color { get; set; }

        [UIHint(UIHint.AllContent)]
        [Display(Name = "Related Item", Order = 99)]
        public virtual ContentReference RelatedItem { get; set; }
    }
}