﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;
using EPiServer.DataAnnotations;
using System;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "F9DA8FA7-65B3-4424-BC2C-7758D49AA850", MetaClassName = "AutomotiveStoreLandingNode")]
    [AvailableContentTypes(Include = new Type[] { typeof(AutomotiveItemContent), typeof(NodeContent) })]
    public class AutomotiveStoreLandingNodeContent : SiteCategoryContent
    {
    }
}