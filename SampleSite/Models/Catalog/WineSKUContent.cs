﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;

namespace EPiServer.Commerce.SampleMvc.Models.Catalog
{
    [CatalogContentType(GUID = "DA90B190-88FC-4DFB-8740-57C0643A4FA0", MetaClassName = "WineSKU")]
    public class WineSKUContent : VariationContent
    {
    }
}