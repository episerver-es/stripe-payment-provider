﻿using EPiServer;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using Mediachase.Commerce.Catalog;
using System;
using System.Web.Mvc;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Web.Routing;
using Mediachase.Commerce;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    [TemplateDescriptor(Inherited = true)]
    [RequireClientResources]
    public class CatalogContentControllerBase<TContent> : ContentController<TContent> where TContent : CatalogContentBase
    {
        private readonly IContentLoader _contentLoader;
        private readonly UrlResolver _urlResolver;

        protected readonly ViewModelFactory _modelFactory;

        public CatalogContentControllerBase()
            : this(ServiceLocator.Current.GetInstance<ViewModelFactory>(),
            ServiceLocator.Current.GetInstance<IContentLoader>(),
            ServiceLocator.Current.GetInstance<UrlResolver>())
        {
        }

        public CatalogContentControllerBase(ViewModelFactory modelFactory, IContentLoader contentLoader, UrlResolver urlResolver)
        {
            _modelFactory = modelFactory;
            _contentLoader = contentLoader;
            _urlResolver = urlResolver;
        }

        protected void ValidEntryOnCurrentMarket(EntryContentBase entry)
        {
            if (entry == null || !entry.IsAvailableInCurrentMarket())
            {
                Response.Redirect(this.MarketNotSupportedPageLink, true);
            }
        }

        private string MarketNotSupportedPageLink
        {
            get
            {
                var homePage = _contentLoader.Get<HomePage>(ContentReference.StartPage);
                var marketNotSupportedPage = homePage.Settings.MarketNotSupportedPage;
                var marketNotSupportedLink = _urlResolver.GetUrl(marketNotSupportedPage);
                return marketNotSupportedLink;
            }
        }
    }
}
