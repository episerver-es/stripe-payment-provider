﻿using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Orders.Dto;
using Mediachase.Commerce.Orders.Managers;
using Mediachase.Commerce.Plugins.Payment;
using Mediachase.Commerce.Website.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    public class SingleShipmentCheckoutPageController : PageController<SingleShipmentCheckoutPage>
    {
        private class ModelWrapper
        {
            public CheckoutViewModel Model { get; set; }
            public Cart Cart { get; set; }
            public DateTimeOffset Now { get; set; }
        }

        private static readonly string _language = "en";
        private static readonly string _paymentPending = PaymentStatus.Pending.ToString();
        private static readonly string _paymentProcessed = PaymentStatus.Processed.ToString();
        private static readonly string _paymentFailed = PaymentStatus.Failed.ToString();
        private static readonly StringComparer _comparer = StringComparer.OrdinalIgnoreCase;

        private readonly PageRouteHelper _pageRouteHelper;
        private readonly ViewModelFactory _viewModelFactory;


        public SingleShipmentCheckoutPageController()
            : this(
            pageRouteHelper: ServiceLocator.Current.GetInstance<PageRouteHelper>(),
            viewModelFactory: ServiceLocator.Current.GetInstance<ViewModelFactory>())
        { }

        public SingleShipmentCheckoutPageController(
            PageRouteHelper pageRouteHelper,
            ViewModelFactory viewModelFactory)
        {
            _pageRouteHelper = pageRouteHelper;
            _viewModelFactory = viewModelFactory;
        }


        public ActionResult Index(SingleShipmentCheckoutPage currentPage)
        {
            var wrapper = GetModelWrapper(currentPage);

            // specify the view explicitly so that other actions can call Index(...).
            return View("Index", wrapper.Model);
        }

        public ActionResult SetShipping(SingleShipmentCheckoutPage currentPage)
        {
            using (var txn = StartTransaction())
            {
                var wrapper = GetModelWrapper(currentPage);

                if (!wrapper.Model.Actions.ContainsKey("SetShipping"))
                {
                    throw new InvalidOperationException("The shipping cannot be set on the current cart.");
                }

                var cart = wrapper.Cart;
                var orderForm = cart.OrderForms.Single();
                var currentMarketId = wrapper.Model.CurrentMarket.MarketId.Value;
                var shippingMethod = ShippingManager.GetShippingMethods(_language).ShippingMethod
                    .Where(m =>
                        m.IsDefault &&
                        m.GetMarketShippingMethodsRows().Any(r => string.Equals(r.MarketId, currentMarketId, StringComparison.OrdinalIgnoreCase)))
                    .Single();

                if (string.IsNullOrEmpty(orderForm.BillingAddressId) ||
                    !cart.OrderAddresses.Any(a => _comparer.Equals(a.Name, orderForm.BillingAddressId)))
                {
                    foreach (OrderAddress existingAddress in cart.OrderAddresses)
                    {
                        existingAddress.Delete();
                    }

                    var address = cart.OrderAddresses.AddNew();
                    PopulateSampleAddress(wrapper, address);

                    orderForm.BillingAddressId = address.Name;
                }

                var shipment = orderForm.Shipments.AddNew();
                shipment.CreatorId = cart.CustomerId.ToString();
                shipment.Created = wrapper.Now.UtcDateTime;
                shipment.ShippingAddressId = orderForm.BillingAddressId;
                shipment.ShippingMethodId = shippingMethod.ShippingMethodId;
                shipment.ShippingMethodName = shippingMethod.Name;

                for (int i = 0; i < orderForm.LineItems.Count; ++i)
                {
                    shipment.AddLineItemIndex(i, orderForm.LineItems[i].Quantity);
                }

                OrderGroupWorkflowManager.RunWorkflow(cart, OrderGroupWorkflowManager.CartPrepareWorkflowName);
                cart.AcceptChanges();
                txn.Complete();
            }

            return Index(currentPage);
        }

        public ActionResult SetPayment(SingleShipmentCheckoutPage currentPage, Guid providerId)
        {
            using (var txn = StartTransaction())
            {
                var wrapper = GetModelWrapper(currentPage);

                if (!wrapper.Model.Actions.ContainsKey("SetPayment"))
                {
                    throw new InvalidOperationException("The payment cannot be set on the current cart.");
                }

                var cart = wrapper.Cart;
                var orderForm = cart.OrderForms.Single();
                var paymentMethod = PaymentManager.GetPaymentMethod(providerId).PaymentMethod.Single();
                var paymentClassType = Type.GetType(paymentMethod.PaymentImplementationClassName);
                var paymentGatewayType = Type.GetType(paymentMethod.ClassName);

                if (IsGenericPayment(paymentGatewayType, paymentClassType))
                {
                    AddNewGenericPayment(wrapper, paymentMethod, paymentClassType);
                }
                else
                {
                    var message = string.Format("Payment gateway not supported: implementation {0}, meta class {1}.", paymentGatewayType.FullName, paymentClassType.FullName);
                    throw new NotSupportedException(message);
                }

                OrderGroupWorkflowManager.RunWorkflow(cart, OrderGroupWorkflowManager.CartCheckOutWorkflowName);

                cart.AcceptChanges();
                txn.Complete();
            }

            return Index(currentPage);
        }

        public ActionResult CompleteOrder(SingleShipmentCheckoutPage currentPage)
        {
            CompletedOrderViewModel model;

            using (var txn = StartTransaction())
            {
                var wrapper = GetModelWrapper(currentPage);

                if (!wrapper.Model.Actions.ContainsKey("CompleteOrder"))
                {
                    throw new InvalidOperationException("The current purchase cannot be completed.");
                }

                var cart = wrapper.Cart;
                OrderGroupWorkflowManager.RunWorkflow(cart, OrderGroupWorkflowManager.CartCheckOutWorkflowName);

                var purchaseOrder = cart.SaveAsPurchaseOrder();
                cart.Delete();
                cart.AcceptChanges();

                model = new CompletedOrderViewModel(currentPage, _viewModelFactory)
                {
                    PurchaseOrderId = purchaseOrder.OrderGroupId,
                    TrackingNumber = purchaseOrder.TrackingNumber
                };

                txn.Complete();
            }

            return View(model);
        }

        public ActionResult Reset(SingleShipmentCheckoutPage currentPage)
        {
            using (var txn = StartTransaction())
            {
                var wrapper = GetModelWrapper(currentPage);
                if (!wrapper.Model.Actions.ContainsKey("Reset"))
                {
                    throw new InvalidOperationException("The current cart cannot be reset.");
                }

                ClearCart(wrapper, false);
                txn.Complete();
            }

            return Index(currentPage);
        }

        public ActionResult Clear(SingleShipmentCheckoutPage currentPage)
        {
            using (var txn = StartTransaction())
            {
                var wrapper = GetModelWrapper(currentPage);
                if (!wrapper.Model.Actions.ContainsKey("Clear"))
                {
                    throw new InvalidOperationException("The current cart cannot be cleared.");
                }

                ClearCart(wrapper, true);
                txn.Complete();
            }

            return Index(currentPage);
        }

        private ModelWrapper GetModelWrapper(SingleShipmentCheckoutPage currentPage)
        {
            var model = new CheckoutViewModel(currentPage ?? PageContext.Page as SingleShipmentCheckoutPage, _viewModelFactory);
            var cart = new CartHelper(Cart.DefaultName).Cart;
            var orderForm = cart.OrderForms.SingleOrDefault();
            var isEmpty = orderForm == null || orderForm.LineItems.Count == 0;
            var hasShipment = !isEmpty && orderForm.Shipments.Any();
            var hasPayment = !isEmpty && orderForm.Payments.Any();

            if (isEmpty)
            {
                model.Actions.Add("Shop", "Add Items to Cart");
            }

            if (!isEmpty && !hasShipment)
            {
                model.Actions.Add("SetShipping", "Add Shipment");
            }

            if (!isEmpty && hasShipment && !hasPayment)
            {
                model.Actions.Add("SetPayment", "Add Payment ({0} payment provider)");
                model.PaymentProviders = PaymentManager.GetPaymentMethods(_language).PaymentMethod
                    .Select(p => new KeyValuePair<Guid, string>(p.PaymentMethodId, p.Name))
                    .ToList();
            }

            if (!isEmpty && hasShipment && hasPayment)
            {
                var payment = orderForm.Payments.Single();
                var paymentStatus = PaymentStatusManager.GetPaymentStatus(payment);

                if (paymentStatus == PaymentStatus.Processed)
                {
                    model.Actions.Add("CompleteOrder", "Complete Order");
                }
            }

            if (!isEmpty && !orderForm.Payments.Any(p => PaymentStatusManager.GetPaymentStatus(p) == PaymentStatus.Processed))
            {
                model.Actions.Add("Clear", "Clear the Cart");
                model.Actions.Add("Reset", "Reset Checkout Process");
            }

            return new ModelWrapper
            {
                Model = model,
                Cart = cart,
                Now = DateTimeOffset.UtcNow
            };
        }

        private void PopulateSampleAddress(ModelWrapper wrapper, OrderAddress address)
        {
            address.CreatorId = wrapper.Cart.CustomerId.ToString();
            address.Created = wrapper.Now.UtcDateTime;
            address.Name = "default";
            address.FirstName = "Firstname";
            address.LastName = "Lastname";
            address.Line1 = "529 14th St NW";
            address.City = "Washington";
            address.State = "DC";
            address.PostalCode = "20045";
        }

        private bool IsGenericPayment(Type paymentGatewayType, Type paymentClassType)
        {
            return
                typeof(GenericPaymentGateway).IsAssignableFrom(paymentGatewayType) &&
                typeof(OtherPayment).IsAssignableFrom(paymentClassType);
        }

        private void AddNewGenericPayment(ModelWrapper wrapper, PaymentMethodDto.PaymentMethodRow paymentMethod, Type paymentClassType)
        {
            var cart = wrapper.Cart;
            var orderForm = cart.OrderForms.Single();

            var payment = orderForm.Payments.AddNew(paymentClassType);
            payment.CreatorId = wrapper.Cart.CustomerId.ToString();
            payment.Created = wrapper.Now.UtcDateTime;
            payment.ImplementationClass = paymentMethod.PaymentImplementationClassName;
            payment.PaymentMethodId = paymentMethod.PaymentMethodId;
            payment.Amount = cart.Total;
            payment.AuthorizationCode = null;
            payment.BillingAddressId = orderForm.BillingAddressId;
            payment.CustomerName = null;
            payment.Status = PaymentStatus.Pending.ToString();
        }

        private void ClearCart(ModelWrapper wrapper, bool deleteLineItems)
        {
            var cart = wrapper.Cart;
            var addresses = cart.OrderAddresses;
            var shipments = cart.OrderForms.SelectMany(x => x.Shipments);
            var payments = cart.OrderForms.SelectMany(x => x.Payments);
            var lineItems = deleteLineItems ? cart.OrderForms.SelectMany(x => x.LineItems) : Enumerable.Empty<LineItem>();

            foreach (var item in addresses.AsEnumerable<OrderStorageBase>().Concat(shipments).Concat(payments).Concat(lineItems))
            {
                item.Delete();
            }

            cart.AddressId = string.Empty;
            cart.AcceptChanges();
        }

        private Mediachase.Data.Provider.TransactionScope StartTransaction()
        {
            return new Mediachase.Data.Provider.TransactionScope();
        }
    }
}