﻿using EPiServer;
using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using Mediachase.Commerce;
using System;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    [TemplateDescriptor(Inherited = true)]
    public class DefaultPageController : PageController<PageData>
    {
        private readonly ICurrentMarket _currentMarket;
        private readonly ViewModelFactory _modelFactory;

        public DefaultPageController()
            : this(ServiceLocator.Current.GetInstance<ViewModelFactory>(), ServiceLocator.Current.GetInstance<ICurrentMarket>())
        {
        }

        public DefaultPageController(ViewModelFactory modelFactory, ICurrentMarket currentMarket)
        {
            _modelFactory = modelFactory;
            _currentMarket = currentMarket;
        }

        public ViewResult Index(PageData currentPage)
        {
            var virtualPath = String.Format("~/Views/{0}/Index.cshtml", currentPage.GetOriginalType().Name);
            if (!System.IO.File.Exists(Request.MapPath(virtualPath)))
            {
                virtualPath = "Index";
            }
            return View(virtualPath, _modelFactory.CreatePageViewModel(currentPage));
        }

        public ActionResult SetMarket(string marketId)
        {
            _currentMarket.SetCurrentMarket(marketId);

            return RedirectToAction("Index");
        }
    }
}
