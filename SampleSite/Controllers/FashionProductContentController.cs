﻿using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.Catalog;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Web.Routing;
using EPiServer.Web;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    [TemplateDescriptor(Inherited = true)]
    [RequireClientResources]
    public class FashionProductContentController : CatalogContentControllerBase<FashionProductContent>
    {
        public FashionProductContentController()
            : base(ServiceLocator.Current.GetInstance<ViewModelFactory>(),
            ServiceLocator.Current.GetInstance<IContentLoader>(),
            ServiceLocator.Current.GetInstance<UrlResolver>())
        {
        }
         
        public FashionProductContentController(ViewModelFactory modelFactory, IContentLoader contentLoader, UrlResolver urlResolver)
            : base(modelFactory, contentLoader, urlResolver)
        {
        }

        public ViewResult Index(FashionProductContent currentContent, string size, string color)
        {

            var model = _modelFactory.CreateFashionViewModel(currentContent);            
                
            var fashionItems = model.Variants.Value
                .Select(x => x.CatalogContent)
                .OfType<FashionItemContent>()
                .ToList();


            var fashionItem = GetFashionItem(size, color, fashionItems);
            ValidEntryOnCurrentMarket(fashionItem);
            model.FashionItemViewModel = _modelFactory.CreateVariationViewModel<FashionItemContent>(fashionItem);

            CreateSelectListItemLists(model, fashionItems);

            return View(model);
        }

        private static void CreateSelectListItemLists(FashionProductViewModel model, List<FashionItemContent> fashionItems)
        {
            if (model.FashionItemViewModel != null)
            {
                model.Color =
                    fashionItems.Select(x => x.Facet_Color)
                                .Distinct()
                                .Select(x => CreateSelectListItem(x, model.FashionItemViewModel.CatalogContent.Facet_Color));
                model.Size =
                    fashionItems.Select(x => x.Facet_Size)
                                .Distinct()
                                .Select(x => CreateSelectListItem(x, model.FashionItemViewModel.CatalogContent.Facet_Size));
            }
        }

        private static FashionItemContent GetFashionItem(string size, string color, IEnumerable<FashionItemContent> fashionItems)
        {
            var fashionItemQuery = fashionItems;
            if (!string.IsNullOrEmpty(size))
            {
                fashionItemQuery = fashionItemQuery.Where(x => x.Facet_Size == size);
            }

            if (!string.IsNullOrEmpty(color))
            {
                fashionItemQuery = fashionItemQuery.Where(x => x.Facet_Color == color);
            }

            return fashionItemQuery.FirstOrDefault();
        }

        private static SelectListItem CreateSelectListItem(string value, string selectedValue)
        {
            return new SelectListItem() { Text = value, Value = value, Selected = value == selectedValue };
        }
    }
}
