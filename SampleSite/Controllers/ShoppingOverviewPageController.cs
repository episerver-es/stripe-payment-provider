﻿using System.Linq;
using System.Web.Mvc;
using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using Mediachase.Commerce;
using Mediachase.Commerce.Catalog;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    public class ShoppingOverviewPageController : PageController<ShoppingOverviewPage>
    {
        private readonly ICurrentMarket _currentMarket;
        private readonly ViewModelFactory _modelFactory;
        private readonly ReferenceConverter _referenceConverter;
        private readonly IContentLoader _contentloader;

        public ShoppingOverviewPageController()
            : this(
            ServiceLocator.Current.GetInstance<ViewModelFactory>(),
            ServiceLocator.Current.GetInstance<ICurrentMarket>(),
            ServiceLocator.Current.GetInstance<ReferenceConverter>(),
            ServiceLocator.Current.GetInstance<IContentLoader>())
        { }

        public ShoppingOverviewPageController(
            ViewModelFactory modelFactory,
            ICurrentMarket currentMarket,
            ReferenceConverter referenceConverter,
            IContentLoader contentloader)
        {
            _modelFactory = modelFactory;
            _currentMarket = currentMarket;
            _referenceConverter = referenceConverter;
            _contentloader = contentloader;
        }

        public ViewResult Index(PageData currentPage)
        {
            var catalogRootLink = _referenceConverter.GetRootLink();
            var firstCatalog = _contentloader.GetChildren<CatalogContent>(catalogRootLink).FirstOrDefault();
            if (firstCatalog != null)
            {
                var model = _modelFactory.CreateCatalogViewModel(firstCatalog, currentPage);
                return View("index", model);
            }
            else
            {
                var model = _modelFactory.CreatePageViewModel(currentPage);
                return View("NoCatalog", model);
            }
        }

        public ActionResult SetMarket(string marketId)
        {
            _currentMarket.SetCurrentMarket(marketId);
            return RedirectToAction("Index");
        }
    }
}
