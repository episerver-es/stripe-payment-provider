﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using System;
using System.Web.Mvc;
using EPiServer.Web.Routing;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    [TemplateDescriptor(Inherited = true)]
    [RequireClientResources]
    public class VariationContentController : CatalogContentControllerBase<VariationContent>
    {
        public VariationContentController()
            : base(ServiceLocator.Current.GetInstance<ViewModelFactory>(),
            ServiceLocator.Current.GetInstance<IContentLoader>(),
            ServiceLocator.Current.GetInstance<UrlResolver>())
        {
        }

        public VariationContentController(ViewModelFactory modelFactory, IContentLoader contentLoader, UrlResolver urlResolver)
            : base(modelFactory, contentLoader, urlResolver)
        {
        }

        public ViewResult Index(VariationContent currentContent, PageData currentPage)
        {
            ValidEntryOnCurrentMarket(currentContent);
            var virtualPath = String.Format("~/Views/{0}/Index.cshtml", currentContent.GetOriginalType().Name);
            if (!System.IO.File.Exists(Request.MapPath(virtualPath)))
            {
                virtualPath = String.Format("~/Views/{0}/Index.cshtml", typeof(VariationContent).Name);
            }

            var model = _modelFactory.CreateVariationViewModel<VariationContent, PageData>(currentContent, currentPage);
            return View(virtualPath, model);
        }
    }
}
