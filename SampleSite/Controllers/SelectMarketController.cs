﻿using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using Mediachase.Commerce;
using System.Linq;
using System.Web.Mvc;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    public class SelectMarketController : Controller
    {
        private readonly Injected<ICurrentMarket> _currentMarket;
        private readonly Injected<UrlResolver> _urlResolver;
        
        [HttpPost]
        public virtual ActionResult Submit(string marketSelector, string contentLink)
        {
            _currentMarket.Service.SetCurrentMarket(marketSelector);
            var currentMarket = _currentMarket.Service.GetCurrentMarket();
            if (currentMarket.Languages.All(l => l.Name != ContentLanguage.PreferredCulture.Name))
            {
                return Redirect(_urlResolver.Service.GetUrl(new ContentReference(contentLink), _currentMarket.Service.GetCurrentMarket().DefaultLanguage.ToString()));
            }
            else
            {
                return Redirect(_urlResolver.Service.GetUrl(new ContentReference(contentLink)));
            }
        }
    }
}
