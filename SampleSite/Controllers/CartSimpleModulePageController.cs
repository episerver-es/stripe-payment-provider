﻿using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Catalog.Managers;
using Mediachase.Commerce.Catalog.Objects;
using Mediachase.Commerce.Orders;
using Mediachase.Commerce.Website.Helpers;
using System;
using System.Web.Mvc;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;

namespace EPiServer.Commerce.SampleMvc.Controllers
{

    [TemplateDescriptor(Inherited = true)]
    public class CartSimpleModulePageController : PageController<CartSimpleModulePage>
    {
        public ViewResult Index(CartSimpleModulePage currentPage)
        {
            return View(new CartModel(currentPage));
        }

        [HttpPost]
        public ActionResult AddToCart(CartSimpleModulePage currentPage, string code, decimal quantity)
        {
            Entry entry = CatalogContext.Current.GetCatalogEntry(code, new CatalogEntryResponseGroup(CatalogEntryResponseGroup.ResponseGroup.CatalogEntryInfo));

            CartHelper ch = new CartHelper(Cart.DefaultName);
            ch.AddEntry(entry, quantity, false);
            ch.Cart.AcceptChanges();

            return  RedirectToAction("Index");
        }

        public ActionResult ClearCart(CartSimpleModulePage currentPage)
        {
            CartHelper ch = new CartHelper(Cart.DefaultName);
            ch.Delete();
            ch.Cart.AcceptChanges();

            return RedirectToAction("Index");
        }
    }
}
