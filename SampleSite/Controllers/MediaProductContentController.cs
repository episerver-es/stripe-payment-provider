﻿using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Commerce.SampleMvc.Models.Catalog;
using EPiServer.Commerce.SampleMvc.Models.PageTypes;
using EPiServer.Commerce.SampleMvc.Models.ViewModels;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web.Mvc;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Web.Routing;

namespace EPiServer.Commerce.SampleMvc.Controllers
{
    [TemplateDescriptor(Inherited = true)]
    [RequireClientResources]
    public class MediaProductContentController : CatalogContentControllerBase<MediaProductContent>
    {
        public MediaProductContentController()
            : base(ServiceLocator.Current.GetInstance<ViewModelFactory>(),
            ServiceLocator.Current.GetInstance<IContentLoader>(),
            ServiceLocator.Current.GetInstance<UrlResolver>())
        {
        }

        public MediaProductContentController(ViewModelFactory modelFactory, IContentLoader contentLoader, UrlResolver urlResolver)
            : base(modelFactory, contentLoader, urlResolver)
        {
        }

        public ViewResult Index(MediaProductContent currentContent, string mediaType)
        {
            var model = _modelFactory.CreateMediaViewModel(currentContent);
                
            var mediaItems = model.Variants.Value
                .Select(x => x.CatalogContent)
                .OfType<MediaItemContent>()
                .ToList();

            var mediaItem = GetMediaItem(mediaItems, mediaType);
            ValidEntryOnCurrentMarket(mediaItem);
            model.MediaItemViewModel = _modelFactory.CreateVariationViewModel<MediaItemContent>(mediaItem);
                
            CreateSelectListItemLists(model, mediaItems);

            return View(model);
        }

        private static void CreateSelectListItemLists(MediaProductViewModel model, IEnumerable<MediaItemContent> mediaItems)
        {
            if (model.MediaItemViewModel != null)
            {
                model.MediaType =
                    mediaItems.Select(x => x.Facet_MediaType)
                                .Distinct()
                                .Select(x => CreateSelectListItem(x, model.MediaItemViewModel.CatalogContent.Facet_MediaType));
            }
        }

        private static MediaItemContent GetMediaItem(IEnumerable<MediaItemContent> mediaItems, string mediaType)
        {
            var mediaItemQuery = mediaItems;
            if (!string.IsNullOrEmpty(mediaType))
            {
                mediaItemQuery = mediaItemQuery.Where(x => x.Facet_MediaType == mediaType);
            }

            return mediaItemQuery.FirstOrDefault();
        }

        private static SelectListItem CreateSelectListItem(string value, string selectedValue)
        {
            return new SelectListItem() { Text = value, Value = value, Selected = value == selectedValue };
        }
    }
}
