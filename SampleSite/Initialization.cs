﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Routing;
using EPiServer.Commerce.SampleMvc.Business;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Framework.Web;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using Mediachase.Commerce;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace EPiServer
{
    [ModuleDependency(typeof(EPiServer.Commerce.Initialization.InitializationModule))]
    public class Initialization : IInitializableModule, IConfigurableModule
    {
        public void Initialize(InitializationEngine context)
        {
            CatalogRouteHelper.MapDefaultHierarchialRouter(RouteTable.Routes, false);
            GlobalFilters.Filters.Add(new HandleErrorAttribute());
            GlobalFilters.Filters.Add(context.Locate.Advanced.GetInstance<PageContextActionFilter>());

            context.Locate.DisplayChannelService().RegisterDisplayMode(new DefaultDisplayMode(RenderingTags.Mobile)
            {
                ContextCondition = (r) => r.GetOverriddenBrowser().IsMobileDevice
            });

            var templateResolver = context.Locate.TemplateResolver();
            templateResolver.TemplateResolved += templateResolver_TemplateResolved;
        }

        public void Preload(string[] parameters)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
            var templateResolver = context.Locate.TemplateResolver();
            templateResolver.TemplateResolving -= templateResolver_TemplateResolved;
        }

        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Container.Configure(c => c.For<ICurrentMarket>().Singleton().Use<CurrentMarketProfile>());
        }

        private void templateResolver_TemplateResolved(object sender, TemplateResolverEventArgs e)
        {
            if (e.RenderType == typeof(RootContent))
            {
                e.SelectedTemplate = null;
            }
        }
    }
}